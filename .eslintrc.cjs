/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution');

module.exports = {
    root: true,
    extends: [
        'plugin:vue/vue3-essential',
        'eslint:recommended',
        '@vue/eslint-config-typescript/recommended'
    ],
    rules: {
        indent: ['error', 4],
        quotes: ['error', 'single'],
        semi: ['error', 'always'],
        'comma-dangle': ['error', 'never'],
        'space-before-function-paren': [0],
        'arrow-parens': [0],
        'no-multiple-empty-lines': ['error', { max: 2, maxBOF: 1 }],
        curly: [2, 'all'],

        // Vue rules
        'vue/component-name-in-template-casing': [
            'error',
            'PascalCase'
            // {
            //     registeredComponentsOnly: true,
            // },
        ],
        'vue/multi-word-component-names': 'off',
        'vue/singleline-html-element-content-newline': [0],
        'vue/multiline-html-element-content-newline': [0],
        'vue/html-self-closing': [0],
        'vue/no-v-html': [0],
        'vue/max-attributes-per-line': [0],
        'vue/html-closing-bracket-newline': [0],
        'vue/html-indent': ['error', 4]
    }
};
