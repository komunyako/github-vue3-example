# github-example

Проект показает какие-то навыки в написании кода.

[Опубликованная версия](https://komunyako.gitlab.io/github-vue3-example/).

Если ломается авторизация, то похоже что cors ругается. Надо [зайти сюда](https://cors-anywhere.herokuapp.com/corsdemo) и нажать кнопку для доступа, потом повторить попытку.
