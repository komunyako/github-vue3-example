import { isUserAuthorized, updateUserData } from '@/services/AuthService';
import { createRouter, createWebHashHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';

export enum ROUTES {
    INDEX = 'index',
    INFO = 'info',
    AUTH = 'auth',
};

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            name: ROUTES.INDEX,
            component: HomeView
        },
        {
            path: '/info',
            name: ROUTES.INFO,
            component: () => import('../views/InfoView.vue'),
            meta: {auth: false}
        },
        {
            path: '/auth',
            name: ROUTES.AUTH,
            component: () => import('../views/AuthView.vue')
        }
    ]
});

router.beforeEach(async(to, from, next) => {
    if (to.meta.auth !== false && !isUserAuthorized.value && to.name !== ROUTES.AUTH) {
        next({name: ROUTES.AUTH})
        
    } else if (to.name === ROUTES.AUTH && isUserAuthorized.value) {
        await updateUserData();

        next({name: ROUTES.INDEX})
    }

    if (isUserAuthorized.value) {
        await updateUserData();
    }
    
    next();
});

export default router;
