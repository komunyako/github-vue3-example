import fetchClient from "@/helpers/fetchClient";
import { useUserStore } from "@/stores/user";
import { computed, ref } from "vue";
import { getAuthorizedUser } from "./GithubService";

const userToken = ref<string|null>(null);
export const isUserAuthorized = computed(() => !!userToken.value);

export function checkToken() {
    const savedToken = window.localStorage.getItem('userToken');

    if (savedToken) {
        setUserToken(savedToken);
    }
}

export function setUserToken(token: string) {
    userToken.value = token;
    // используем localStorage потому что нет сервера и куки не требуются
    window.localStorage.setItem('userToken', token);
}

export function getUserToken() {
    if (!userToken.value) {
        checkToken();
    }

    return userToken.value;
}

export async function getUserData() {
    const userStore = useUserStore();
    
    if (!userStore.isAuthorized) {
        const [user] = await getAuthorizedUser();
        
        if (user) {
            userStore.doUpdateUser(user);
        }
    }

    return userStore.user;
}

export async function updateUserData() {
    const userStore = useUserStore();

    if (!userStore.isAuthorized) {
        await getUserData();
    }
}

