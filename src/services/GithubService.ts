import { apiFetch } from "@/helpers/apiFetch";
import fetchClient from "@/helpers/fetchClient";
import type { UserID, UserProfile, UserSimple } from "@/models/User";
import { setUserToken } from "./AuthService";

export function startGithubAuth() {
    const authUrl = new URL('https://github.com/login/oauth/authorize');
    authUrl.searchParams.append('client_id', import.meta.env.CLIENT_GITHUB_KEY);
    authUrl.searchParams.append('scope', 'read:user');

    window.open(authUrl, '_blank', 'popup=yes,width=500,height=500');
}

export async function authorizeThroughGithub(code: string) {
    const [data, error] = await apiFetch(() => fetchClient.post<{ access_token: string }>('https://cors-anywhere.herokuapp.com/https://github.com/login/oauth/access_token', {
        client_id: import.meta.env.CLIENT_GITHUB_KEY,
        client_secret: import.meta.env.CLIENT_GITHUB_SECRET,
        code
    }));

    if (data) {
        setUserToken(data.access_token);

        return [data.access_token];
    }

    return [null, error];
}

export async function getAuthorizedUser() {
    return apiFetch<UserProfile>(() => fetchClient.get('https://api.github.com/user'));
}

export async function getUsers(params: {per_page?: number, since: UserID}) {
    return apiFetch<UserSimple[]>(() => fetchClient.get('https://api.github.com/users', {params: {per_page: 10, ...params}}));
}
