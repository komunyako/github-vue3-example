import type { UserProfile } from '@/models/User';
import { defineStore } from 'pinia';
import { computed, ref } from 'vue';

export const useUserStore = defineStore('user-store', () => {
    const userData = ref<UserProfile | null>(null)
    const isUserAuthorized = computed(() => !!userData.value);

    const doUpdateUser = (user: UserProfile) => {
        userData.value = user;
    };

    return {
        user: userData,
        isAuthorized: isUserAuthorized,
        doUpdateUser
    };
});
