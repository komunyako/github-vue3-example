import { getUserToken, isUserAuthorized } from "@/services/AuthService";
import axios from "axios";

const fetchClient = axios.create({
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    }
});

fetchClient.interceptors.request.use(
    (config) => {
        if (isUserAuthorized.value) {
            config.headers = {
                ...config.headers,
                Authorization: 'token ' + getUserToken()
            };
        }
        
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

fetchClient.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        // Обработать неавторизованный запрос
        return Promise.reject(error);
    }
);

export default fetchClient;
