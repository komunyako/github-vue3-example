import type { AxiosError, AxiosResponse } from "axios";

export type FetchResult<T> = Promise<[T|null, AxiosError|null]>;

export async function apiFetch<T>(fetchFunction: ()=> Promise<AxiosResponse<T>>): FetchResult<T> {
    try {
        const result = await fetchFunction();

        return [result.data, null];

    } catch (error) {
        return [null, error as AxiosError];
    }
}
