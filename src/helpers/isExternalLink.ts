
const baseURL = new URL(`${window.location.protocol}//${window.location.host}`);

const isExternalLink = (link: string) => {
    return new URL(link, baseURL).hostname !== baseURL.hostname;
};

export default isExternalLink;
